import styled from 'styled-components';

export const Card = styled.div`
    background-color: gray;
    height: 300px;
    width: 100%;
    border: 1px solid gray;
    border-radius: 20px;
    display: flex;
    justify-content:center;
    align-items: center;
`;
