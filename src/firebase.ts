import firebase from 'firebase'

export const firebaseConfig = {
    apiKey: "AIzaSyAspI6QIhd1v43vNR8V0wpJW4dL6pey0TQ",
    authDomain: "droov-fbaef.firebaseapp.com",
    databaseURL: "https://droov-fbaef.firebaseio.com",
    projectId: "droov-fbaef",
    storageBucket: "droov-fbaef.appspot.com",
    messagingSenderId: "93078866564",
    appId: "1:93078866564:web:bf2a67542dd66102ded0d1",
    measurementId: "G-JMRRRNNM3J"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default firebase;
