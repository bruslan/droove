import React, { useEffect } from 'react';
import { Card } from './Components/Card';
import firebase from './firebase'


function App() {

  const db = firebase.firestore()


  useEffect(() => {

    db.collection("products").get().then(docSnapshot => {
      docSnapshot.forEach((doc: any) => {
        // doc.data() is never undefined for query doc snapshots
        console.log(doc.id, " => ", doc.data());
        // Do stuff here
      });
    }).catch(error => console.log("Error cant download this stuff", error))

    return () => {

    }
  }, [])

  return (
    <div className="App">
      <h1>Droove V3</h1>
      <Card>
        Hi im a Card
      </Card>


    </div>
  );
}

export default App;
